var ACTIONS = {
    ADDJADE: "ADD_JADE",
    GIVEDISC: "GIVE_DISCOUNT",
    WINSPECIAL: "WIN_SPECIAL_PRIZE"
}

var SPECIAL = {
    CAR: "new_car",
    BOAT: "cruise_travel",
    PLANE: "plane_travel",
    GEM: "free_gem",
    GIFT: "special_gift"
}

var EVENTS = {
    GAMESTART : "GAME_START",
    GAMEEND : "GAME_END"
}

//----------------------------- PREMIOS -----------------------------
var PREMIOS = {
    points : [
        {id: "jade1k", img: "1-mil-jades.png", prize: {action: ACTIONS.ADDJADE, cantidad: 1000}}, 
        {id: "jade2k", img: "2-mil-jades.png", prize: {action: ACTIONS.ADDJADE, cantidad: 2000}}, 
        {id: "jade3k", img: "3-mil-jades.png", prize: {action: ACTIONS.ADDJADE, cantidad: 3000}},
        {id: "jade4k", img: "4-mil-jades.png", prize: {action: ACTIONS.ADDJADE, cantidad: 4000}}, 
        {id: "jade5k", img: "5-mil-jades.png", prize: {action: ACTIONS.ADDJADE, cantidad: 5000}}
    ],

    discounts: [
        {id: "desc10", img: "10-des.png", prize: {action: ACTIONS.GIVEDISC, percent: 10}}, 
        {id: "desc20", img: "20-des.png", prize: {action: ACTIONS.GIVEDISC, percent: 20}}, 
        {id: "desc30", img: "30-des.png", prize: {action: ACTIONS.GIVEDISC, percent: 30}}, 
        {id: "desc40", img: "40-des.png", prize: {action: ACTIONS.GIVEDISC, percent: 40}}, 
        {id: "desc50", img: "50-des.png", prize: {action: ACTIONS.GIVEDISC, percent: 50}}
    ],

    prizes: [
        {id: "super1", img: "super-1.png", prize: {action: ACTIONS.WINSPECIAL, identifier: SPECIAL.CAR}}, 
        {id: "super2", img: "super-2.png", prize: {action: ACTIONS.WINSPECIAL, identifier: SPECIAL.BOAT}}, 
        {id: "super3", img: "super-3.png", prize: {action: ACTIONS.WINSPECIAL, identifier: SPECIAL.PLANE}}, 
        {id: "super4", img: "super-4.png", prize: {action: ACTIONS.WINSPECIAL, identifier: SPECIAL.GEM}}, 
        {id: "super5", img: "super-5.png", prize: {action: ACTIONS.WINSPECIAL, identifier: SPECIAL.GIFT}}
    ]
}

var BILLBOARD = {
    lang : {
        es : 3,
        pt : 2,
        fr : 2,
        en : 2
    },
    top : "mensaje-su",
    bot : "mensaje-in-"
}

/*var PREMIOS = {
    points : [
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
    ],

    discounts: [
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
    ],

    prizes: [
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
        {id: "jade1k", img: "1-mil-jades.png"}, 
    ],
}*/

//----------------------------- CONSTANTES ---------------------------
var ORIENTATION = {
    LANDSCAPE : "landscape",
    PORTRAIT : "portrait"
}

var MODE = {
    TESTING : "testing",
    DEPLOYMENT : "deployment"
}

var PARAMETERS = {
    LANGUAGE : ["l_id", "li", "lang_id"],
    VIDENTE : "v_id",
}

var KEYWORDS = {
    TOP : "top", 
    BOTTOM : "bottom"
}

var DIR = {
    IMG_PREMIOS : "img/premios/"
}

//-----------------------------------------------------------------------
var Game = {
    language : 0,
    language_string_resource : new Array(),
    elements : {main : "", sub : ""},
    objs: {},
    extraelements : {x: new Array(), y: new Array()},
    options : null,
    orientation : ORIENTATION.LANDSCAPE,
    mode : "",
    trackontesting : false,
    init : function(opts){
        this.setLanguage();
        this.setOptions(opts);
    },
    loadStart : function(){
        try{
            this.get("loading").classList.remove("bgload");
            this.get("loading").classList.add("bgunload");
        }catch(err){
            this.log("undefined loadingscreen start");
        }
    },
    loadEnd : function(){
        try{
            this.get("loading").classList.remove("bgunload");
            this.get("loading").classList.add("bgload");
        }catch(err){
            this.log("undefined loadingscreen end");
        }
    },
    deployNotification: function(type, content){
        this.get("notoverlay").classList.remove("bgload");
        this.get("notoverlay").classList.add("bgunload");
        
        var ctx = this;

        ctx.get("notification").classList.remove("minimize");
        ctx.get("notification").classList.add("maximize");

        setTimeout(function(){
            ctx.get("notification").classList.add("rubberBand");
            setTimeout(function(){
                ctx.get("notshadow").classList.remove("glow");
                ctx.get("notshadow").classList.add("glow");
            }, 1000);
        }, 350);
    },
    setLanguage : function(){
        if(typeof(PARAMETERS.LANGUAGE) == "object"){
            for(var i = 0; i < PARAMETERS.LANGUAGE.length; i++){
                if(!this.language){
                    this.language = Utils.getQueryVariable(PARAMETERS.LANGUAGE[i]);
                }else{
                    return;
                }
            }
        }else{
            this.language = Utils.getQueryVariable(PARAMETERS.LANGUAGE);
            if(!this.language){
                this.language = 1;
            }
        }
    },
    refreshContainer : function(){
        winWidth = window.innerWidth;
        winHeight = window.innerHeight;

        exheight = this.getExtendedHeight();

        if(winWidth < winHeight){
            this.orientation = ORIENTATION.PORTRAIT;
            this.get("main").style.width = winWidth * this.options.dim.portrait.width +  "px";
            this.get("main").style.height = winWidth * this.options.dim.portrait.height + "px";
        }else{
            this.orientation = ORIENTATION.LANDSCAPE;
            // Width se calcula en base al Height del contenedor
            this.get("main").style.width = winHeight * this.options.dim.landscape.width + "px";
            this.get("main").style.height = winHeight * this.options.dim.landscape.height + exheight + "px";
        }
    },
    setOptions : function(opts){
        this.options = opts;
        
        this.mode = opts.mode;

        this.setElements();

        if(typeof(opts.lanopts) != "undefined"){
            this.language_string_resource = opts.lanopts[this.language];
        }
        
        if(typeof(opts.dim) != "undefined"){
            this.refreshContainer();
        }

        if(typeof(opts.border) != "undefined"){
            bopts = opts.border;
            var b;

            if(typeof(bopts.customId) == "undefined"){
                b = document.getElementById("game-border");
            }else{
                b = document.getElementById(bopts.customId);
            }

            View.setPercDim(b, this.get("main").offsetWidth, 1, this.get("main").offsetHeight, 1);
            
            boffsettop = this.get("main").offsetHeight * bopts.offsetY;
            boffsetleft = this.get("main").offsetWidth * bopts.offsetX;

            nc_dim_w = this.get("main").offsetWidth * (1 - bopts.offsetX * 2);
            nc_dim_h = this.get("main").offsetHeight * (1 - bopts.offsetY * 2);
            
            View.setPercDim(this.get("sub"), nc_dim_w, 1, nc_dim_h, 1);
            
            this.get("sub").style.minHeight = nc_dim_h - boffsettop + "px";
            this.get("sub").style.minWidth = nc_dim_w - boffsetleft + "px";

            this.get("sub").style.top = boffsettop + "px";
            this.get("sub").style.left = boffsetleft + "px";
        }

        if(typeof(opts.loading) != "undefined"){
            if(typeof(opts.loading.id) != "undefined"){
                this.addElement("loading", opts.loading.id);
            }else{
                var loadingele = "<div id=\"blackoverlay\"></div>";
                if(typeof(opts.loading.container) == "undefined"){
                    Utils.addInner(this.get("main"), loadingele, KEYWORDS.TOP);
                }else{
                    Utils.addInner(this.get(opts.loading.container), loadingele, KEYWORDS.TOP);
                }
                
                this.addElement("loading", "blackoverlay");
            }
        }

        if(typeof(opts.notification) != "undefined"){
            if(typeof(opts.notification.id) != "undefined"){
                this.addElement("notoverlay", opts.notification.overlay);
                this.addElement("notification", opts.notification.id);
                this.addElement("notshadow", opts.notification.shadow);
            }else{
                var loadingele = "<div id=\"gm_notification\"></div>";
                Utils.addInner(this.get(opts.notification.container), loadingele, KEYWORDS.TOP);
                this.addElement("notification", "gm_notification");
            }            
        }

        if(typeof(trackontesting) != "undefined"){
            this.trackontesting = opts.trackontesting;
        }
    },
    setElements : function(){
        ele = this.options.elements;
        ctx = this;

        for(var key in ele){
            ctx.objs[key] = document.getElementById(ele[key]);
        }
    },
    addElement : function(key, el){
        if(typeof(el) == "string"){
            this.options.elements[key] = el;
            this.objs[key] = document.getElementById(el);
        }else{
            this.options.elements[key] = el.id;
            this.objs[key] = el;
        }
    },
    get : function(key){
        return this.objs[key];
    },
    sendMsg : function(ev, content){
        var msg = {event: ev, data: content};
        window.parent.postMessage(msg, "*");
    },
    msgHandler : function(msg){
        if(msg.data != null && msg.data != "undefined"){
            var dat = msg.data;
            
            Game.log(dat);
        }
    },
    buildMsg : function(idarray, separator){
        var ctx = this;
        if(typeof(idarray) == "object"){
            var msg = "";
            var spnum = idarray.length;
            idarray.forEach(function(id){
                if(typeof(separator) != "undefined"){
                    spnum -= 1;
                    if(spnum > 0){
                        msg += ctx.language_string_resource[id] + separator;
                    }else{
                        msg += ctx.language_string_resource[id];
                    }
                }else{
                    msg += ctx.language_string_resource[id];
                }
            });
            return msg;
        }else{
            return this.language_string_resource[idarray];
        }
    },
    getExtendedHeight : function(){
        if(typeof(this.options.ex_ele_h) != "undefined"){
            eh = 0;
            this.options.ex_ele_h.forEach(function(hel){
                eh += document.getElementById(hel).offsetHeight;
            });
            return eh;
        }else{
            return 0;
        }
    },
    log : function(message){
        if(this.mode == MODE.TESTING){
            console.log(message);
        }
    },
    track : function(action, properties){
        try{
            if(typeof(analytics) == "undefined"){
                if(this.mode != MODE.TESTING || this.trackontesting == true){
                    analytics.track(this.options.track, action, properties);
                }
            }
        }catch(err){
            this.log(err);
            this.log("analytics error!");
        }
    }
}

var Utils = {
    getQueryVariable : function(variable){
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
        }
        return(false);
    },
    getCurrentDate : function(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = parseInt(today.getFullYear());
        var date = {
            day:dd,
            month:mm,
            year:yyyy
        }
        return date;
    },
    setInner : function(elid, txt){
        if(typeof(elid) == "string"){
            document.getElementById(elid).innerHTML = txt;
        }else if(typeof(elid) == "object"){
            elid.innerHTML = txt;
        }

        Game.setElements();
    },
    addInner : function(elid, txt, place){
        var tempcont;
        var tele;

        if(typeof(elid) == "string"){
            tele = document.getElementById(elid);
            tempcont = tele.innerHTML;
        }else if(typeof(elid) == "object"){
            tele = elid;
            tempcont = tele.innerHTML;
        }

        if(place == KEYWORDS.BOTTOM){
            tele.innerHTML = (tempcont+txt);
        }else if(place == KEYWORDS.TOP){
            tele.innerHTML = (txt + tempcont);
        }

        Game.setElements();
    },
    preventOnDrag : function(elems){
        elems.forEach(function (el){
            if(el != null && typeof(el) !== "undefined"){
                el.ondragstart = function(){ return false;};
            }
        });
    },
    getRandomFromRange : function(min, max){
        return Math.floor(Math.random() * (max-min)) + min;
    },
    toRadians : function(degrees) {
        return degrees * Math.PI / 180;
    },
    toDegrees : function(radians) {
        return radians * 180 / Math.PI;
    },
    validateEmail : function(email) {
		return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email);
    },
    validateName : function(vname){
        return /^[a-zàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœA-Z ]+$/.test(vname)
    }
}

var View = {
    setPercDimAxis : function(el, ref, perc, axis){
        
        if(axis === "width"){
            el.style.width = ref*perc + "px";
        }else if(axis === "height"){
            el.style.height = ref*perc + "px";
        }
    },
    setPercDim : function(el, refx, percx, refy, percy){
        el.style.width = refx*percx + "px";
        el.style.height = refy*percy + "px";
    },
    centerElement : function(element, width, height){
        var centerX = width/2;
        var centerY = height/2;

        var offsetX = parseFloat(element.style.width)/2;
        var offsetY = parseFloat(element.style.height)/2;

        element.style.left = centerX - offsetX + "px";
        element.style.top = centerY - offsetY-7 + "px";
    },
    centerElement : function(element, offset){
        element.style.left = (offset/2) - 3 + "px";
        element.style.top = (offset/2) - 2 + "px";
    },
    centerVElement : function(el, height){
    var centerY = height/2;
    var offsetY = parseFloat(el.offsetHeight)/2;

    el.style.top = centerY-offsetY + "px";
    },
    centerVBetweenElements : function(ele, topEl, botEl, height){
        var topElbot = parseFloat(topEl.style.height)/2;
        var botEltop = height - parseFloat(botEl.style.height)/2;

        ele.style.top = botEltop;
    },
    centerHElement : function(element, width){
        var centerX = width/2;
        var offsetX = parseFloat(element.style.width)/2;

        element.style.left = centerX-offsetX + "px";
    },
    positionHalfOffset : function(type, element, height, offset){
        var elcenY = parseFloat(element.style.height)/2;

        if(type === "top"){
            element.style.top = offset - elcenY + "px";
        }else if(type === "abstop"){
        element.style.top == offset + 0 + "px";
        }else if(type === "bottom"){
            element.style.bottom = -elcenY + "px";
        }else if(type === "absbottom"){
            element.style.bottom = offset + 0 + "px";
        }
    }
}