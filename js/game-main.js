(function(){
    var winWidth, winHeight;
    var firstTimeLoading = true;

    var supportsOrientationChange = "onorientationchange" in window; 
    var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

    var ANIMATIONS = {topWheelAnim:"", botWheelAnim:""};
	var animNames = {topWheelAnim:"topRotateTo", botWheelAnim:"botRotateTo"};

    var zodImgRES = ["1-mil-jades.png", "2-mil-jades.png", "3-mil-jades.png", "4-mil-jades.png", "5-mil-jades.png", "5-mil-jades.png", "4-mil-jades.png", "3-mil-jades.png", "2-mil-jades.png", "1-mil-jades.png"];
    
    var frmes = {
            zodNames : ["Aries", "Tauro", "Geminis", "Cancer", "Leo", "Virgo", "Libra", "Escorpio", "Sagitario", "Capricornio", "Acuario", "Piscis"],
            nahNames : ["Aries", "Tauro", "Geminis", "Cancer", "Leo", "Virgo", "Libra", "Escorpio", "Sagitario", "Capricornio", "Acuario", "Piscis", "Test2", "Test3", "Test4", "Test5", "Test6", "Test7", "Test8", "Test9"]
	};

	var frmen = {
            zodNames : ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces"]
	};

	var frmfr = {
            zodNames : ["Bélier", "Taureau", "Gémeaux", "Cancer", "Lion", "Vierge", "Balance", "Scorpion", "Sagittaire", "Capricorne", "Verseau", "Poissons"]
	};

	var frmbr = {
            zodNames : ["Áries", "Touro", "Gêmeos", "Câncer", "Leão", "Virgem", "Libra", " Escorpião", "Sagitário", "Capricórnio", "Aquário", "Peixes"]
	};

    var lanopts = {
        1 : frmes, 
        2 : frmbr, 
        3 : frmfr, 
        4 : frmen
    };

    var prizenum = 10;
    var rotation = 0;
    var radius = 202;
    var step = (2*Math.PI) / 10, angle = 0;
    var resizeF;
    var wheelEl = new Array();

    var gameopts = {
        elements : {
            main : "game_container", 
            sub: "sub_container", 
            wheelcont: "pinwheel", 
            pivot: "pivot", 
            l1: "light_pattern_1",
            l2: "light_pattern_2",
            la: "light_pattern_all"
        },
        mode: MODE.TESTING,
        loading : {container: "sub"}, // id:custom ele id, container:identificador del contenedor para el loading screen(default:main), contenedor ya debe estar declarado en elements..
        notification: {overlay: "notbox", id: "notif", shadow: "notifshadow"},
        dim : {
            portrait:{width: 1, height: 1.367536}, // height = width%
            landscape:{width: 0.6896551, height: 1} // width = height%
        },
        /*border: {offsetY: 0.024, offsetX: 0.0405},*/ //opciones: offsetY, offsetX, customId;
        ex_ele_h : [], // id de los elementos que extienden el layout verticalmente
        lanopts : lanopts,
        track : "Game Fortune Wheel",
        trackontesting : false
    };

    function setGameContainer(event){
        nc_dim_h = Game.get("sub").offsetHeight;
		nc_dim_w = Game.get("sub").offsetWidth;

        Game.get("main").classList.remove("invis");

        Game.refreshContainer();
        init(event);
        
        Game.get("pivot").onclick = function(){
            Game.get("pivot").classList.remove("btni");
            Game.get("pivot").classList.add("btna");

            var rid = reRandom();

            try{
                Game.get("wheelcont").classList.remove("rmovewheel");
            }catch(er){
                Game.log(er);
            }
            
            addAnimPrefixCompat("#pinwheel", animNames.botWheelAnim, Utils.toDegrees(step) * rid);
            Game.get("wheelcont").classList.add("rmovewheel");

            /*setTimeout(function(){
                flickerLights();
                setTimeout(function(){
                    Game.sendMsg(EVENTS.GAMEEND, JSON.stringify(wheelEl[rid]));
                    Game.deployNotification("", "");
                }, 1000);                
            }, 2500);*/
        };

        loadingComplete();
    }

    function flickerLights(){
        Game.get("l1").classList.remove("lfadein");
        Game.get("l2").classList.remove("rfadein");

        Game.get("l1").classList.add("off");
        Game.get("l2").classList.add("off");

        Game.get("la").classList.remove("off");
        Game.get("la").classList.add("flicker");
    }

    window.addEventListener("message", function(event){
        Game.msgHandler(event);
	});

    function reRandom(){
        var winid = Utils.getRandomFromRange(0, prizenum);
        if(winid == 1 || winid == 9){
            return reRandom();
        }else{
            return winid;
        }
    }

    function loadingComplete(){
		 Game.loadEnd();
		 window.scrollBy(0,1);
		 window.scrollBy(0,-1);
	 }

    /*function initUIElements(){
        mainContainer = document.getElementById("game_container");
    }*/

    function loadGame(){
        if(firstTimeLoading){
            Game.init(gameopts);
            Game.loadStart();
			//Utils.preventOnDrag([topWheel, botWheel, topWCenter, botWCenter, rib_top, rib_bot, btn_go, datepicker]);

            window.addEventListener(orientationEvent, function(){
                Game.loadStart();

                typeof(resize) != "undefined" ? clearTimeout(resize) : false;                    

                resize = setTimeout(function(){
                    setGameContainer(orientationEvent);
                }, 700);
            });

            firstTimeLoading = false;
        }

    	setTimeout(function(){
            setGameContainer("init");
        }, 200);
    }

    function getRandomElements(arr, n){
        arr.sort(function(){ return 0.5 - Math.random()});
        arr.sort(function(){ return 0.5 - Math.random()});
        arr.sort(function(){ return 0.5 - Math.random()});

        r = new Array();
        for(var i = 0; i < n; i++){
            r.push(arr[i]);
        }

        //console.log(r);

        return r;
    }

    //GAME BEHAVIOR LOGIC
    function init(event){
            var width = Game.get("wheelcont").offsetWidth;
            var height = Game.get("wheelcont").offsetHeight;
            var specpriz = "";

            angle = step*2.5*-1;
            
            if(event === "init"){
                wheelEl = getRandomElements(PREMIOS.points, 4).concat(getRandomElements(PREMIOS.discounts, 4));

                specpriz = getRandomElements(PREMIOS.prizes, 2);

                wheelEl = getRandomElements(wheelEl, wheelEl.length);

                wheelEl.splice(1, 0, specpriz[0]);
                wheelEl.push(specpriz[1]);

                Game.addElement("wheelobjs", wheelEl);
            }

            
            View.setPercDim(Game.get("pivot"), Game.get("main").offsetWidth, 0.25, Game.get("main").offsetWidth, 0.25);

            var id = "";
            Game.log("------");
            wheelEl.forEach(function(item, index){
                id = item.id;
                
                if(event === "init"){
                    Utils.addInner(Game.get("wheelcont"), '<img draggable="false" class="wheelelement" id="' + id + '" src="' + DIR.IMG_PREMIOS + item.img + '"></img>', KEYWORDS.TOP);
                    Game.addElement(id, id);
                }  

                Game.log(id);
                View.setPercDimAxis(Game.get(id), Game.get("main").offsetWidth, 0.44, "width");
                View.setPercDimAxis(Game.get(id), Game.get(id).offsetWidth, 1.616, "height");

                radius = Game.get(id).offsetHeight/2;

                var x = Math.round(width/2 + radius * Math.cos(angle) - Game.get(id).offsetWidth/2);
                var y = Math.round(height/2 + radius * Math.sin(angle) - Game.get(id).offsetHeight/2);

                rotation = Utils.toDegrees(angle) + 90;

                Game.get(id).style.left = x + "px";
                Game.get(id).style.top = y + "px";
                Game.get(id).style.transform = "rotate(" + rotation + "deg)";
                
                angle += step;
            });
            Game.log("------");
    }

    function addAnimPrefixCompat(elid, animationame, ndeg){
		animation = false,
		animationstring = 'animation',
		prefix = '',
		domPrefixes = 'Webkit Moz O ms'.split(' '),
		pfx  = '',
		keyframe = '',
		elm = document.querySelector(elid);

		if( elm.style.animationName ) { animation = true; }

		if( animation === false ) {
			for( var i = 0; i < domPrefixes.length; i++ ) {
				if( elm.style[ domPrefixes[i] + 'AnimationName'] !== undefined ) {
					pfx = domPrefixes[i];
					prefix = '-' + pfx.toLowerCase() + '-';
					animationstring = prefix + 'animation';
					animation = true;
					//initWheelAnimations(prefix, nahual);
                    //var rotan = (-3600-ndeg+(Utils.toDegrees(step)*3.5));
                    var rotan = -3600-ndeg;
                    Game.log(-360-ndeg);
                    Game.log(rotan);
                    //Game.log(ndeg+Utils.toDegrees(step)/2);
					if(animationame === animNames.topWheelAnim){
						keyframe = '@' + prefix + 'keyframes '+ animNames.topWheelAnim +'{ '+
						'0% { ' + prefix + 'translate(-50%, -50%) transform: rotate(0deg);}'+
						'100% { ' + prefix + 'transform:translate(-50%, -50%) rotate(' + (3600+ndeg+step*5) + 'deg);}'+
						'}';                                      
					}else if(animationame === animNames.botWheelAnim){
                        //-3600-ndeg-step*5
						keyframe = '@' + prefix + 'keyframes '+ animNames.botWheelAnim +'{ '+
						'0% { ' + prefix + 'transform:translate(-50%, -50%) rotate(0deg);}'+
                        '100% { ' + prefix + 'transform:translate(-50%, -50%) rotate(' + rotan + 'deg);}'+
						'}';
					}
					document.styleSheets[0].insertRule(keyframe, 0);
				}
			}
		}
	 }

    loadGame();
}).call(this);